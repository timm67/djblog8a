# djblog8a

## ModelForm
This django site implements a ModelForm at /add which allows
the user to add a new post via a form. XSS protection of the text 
payload is included. 

## Rest Framework

I tried to add _both_ RSS feed and, more importantly Rest Framework.
The code is there for the Rest Framework and I believe the serialization
hooks are correct. However, I was not able to plumb in the /api URL and
have it call into the Rest Framework bits to output serialized Post
data. 
