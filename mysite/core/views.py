from rest_framework import viewsets
from blogging.models import Post, Category
from .serializers import PostSerializer, CategorySerializer

class PostViewSet(viewsets.ModelViewSet):
    published_posts = Post.objects.exclude(published_date__exact=None)
    posts = published_posts.order_by('-published_date')
    serializer_class = PostSerializer

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
# Create your views here.
