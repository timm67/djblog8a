from django.conf.urls import url
from rest_framework import routers
from core.views import PostViewSet, CategoryViewSet

router = routers.DefaultRouter()
router.register(r'posts', PostViewSet)
router.register(r'categories', CategoryViewSet)

urlpatterns = router.urls
