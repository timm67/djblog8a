from django.urls import include, path
from blogging.views import list_view, detail_view, add_view
from rest_framework import routers
from .views import PostViewSet, CategoryViewSet

router = routers.DefaultRouter()
router.register(r'api_post', PostViewSet)
router.register(r'api_category', CategoryViewSet)

urlpatterns = [
    path('', list_view, name="blog_index"),
    path('posts/<int:post_id>/', detail_view, name="blog_detail"),
    path('add/', add_view, name="blog_add"),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
